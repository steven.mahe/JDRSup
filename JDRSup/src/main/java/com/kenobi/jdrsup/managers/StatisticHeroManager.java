package com.kenobi.jdrsup.managers;

import com.kenobi.jdrsup.managers.base.BaseManager;
import com.kenobi.jdrsup.managers.interfaces.IStatisticCharacter;
import com.kenobi.jdrsup.models.StatisticHero;

public class StatisticHeroManager extends BaseManager<StatisticHero> implements IStatisticCharacter{

}
