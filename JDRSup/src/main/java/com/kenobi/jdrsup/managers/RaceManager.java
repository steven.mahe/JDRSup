package com.kenobi.jdrsup.managers;

import com.kenobi.jdrsup.managers.base.BaseManager;
import com.kenobi.jdrsup.managers.interfaces.IRaceManager;
import com.kenobi.jdrsup.models.Race;

public class RaceManager extends BaseManager<Race> implements IRaceManager{

}
