package com.kenobi.jdrsup.managers.interfaces.base;

import java.util.List;

import com.kenobi.jdrsup.models.base.BaseEntity;

public interface IBaseManager<T extends BaseEntity> {
	
	public void create(T item);

	public void delete(T item);

	public void delete(Integer id);

	public List<T> getAll();

	public T getById(Integer id);

	public void update(T item);
}
