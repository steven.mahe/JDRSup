package com.kenobi.jdrsup.managers;

import com.kenobi.jdrsup.managers.base.BaseManager;
import com.kenobi.jdrsup.managers.interfaces.IProfessionManager;
import com.kenobi.jdrsup.models.Profession;

public class ProfessionManager extends BaseManager<Profession> implements IProfessionManager{

}
