package com.kenobi.jdrsup.managers;

import com.kenobi.jdrsup.managers.base.BaseManager;
import com.kenobi.jdrsup.managers.interfaces.ISkillManager;
import com.kenobi.jdrsup.models.Skill;

public class SkillManager extends BaseManager<Skill> implements ISkillManager{

}
