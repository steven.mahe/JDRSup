package com.kenobi.jdrsup.managers;

import com.kenobi.jdrsup.managers.base.BaseManager;
import com.kenobi.jdrsup.managers.interfaces.IStatisticManager;
import com.kenobi.jdrsup.models.Statistic;

public class StatisticManager extends BaseManager<Statistic> implements IStatisticManager{

}
