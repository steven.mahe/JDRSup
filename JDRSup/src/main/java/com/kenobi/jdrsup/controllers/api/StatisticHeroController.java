package com.kenobi.jdrsup.controllers.api;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kenobi.jdrsup.managers.interfaces.base.IBaseManager;
import com.kenobi.jdrsup.models.StatisticHero;

@RestController
@RequestMapping("/statisticHero")
public class StatisticHeroController {
	@Autowired
    private IBaseManager<StatisticHero> manager;

	/**
	 * List of all general skill types
	 * 
	 */
    @RequestMapping(value="/", method=RequestMethod.GET)
    public List<StatisticHero> getAll() {
    	 
  
        return this.manager.getAll();
    }

	/**
	 * Get a general skill type by id
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public StatisticHero get(@PathVariable Integer id, HttpServletResponse response) {
        StatisticHero entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

        return entity;
    }

	/**
	 * Delete a general skill type
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public StatisticHero delete(@PathVariable Integer id) {
        StatisticHero generalType = this.manager.getById(id);

        if (generalType != null) {
            this.manager.delete(generalType);
        }

        return generalType;
    }

	/**
	 * Create a general skill type
	 * 
	 * @param
	 */
    @RequestMapping(value="/", method=RequestMethod.POST)
    public StatisticHero create(@RequestParam String name) {
        StatisticHero entity = new StatisticHero();

        entity.setName(name);

        this.manager.create(entity);

        return entity;
    }

	/**
	 * Update a general skill type
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public StatisticHero update(HttpServletResponse response, @PathVariable int id, @RequestParam String name) {
        StatisticHero entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else if(name != null && !name.equals(entity.getName())) {
            entity.setName(name);

            this.manager.update(entity);
        } else {
            response.setStatus(418);
        }

        return entity;
    }

    @RequestMapping(value="/fill", method=RequestMethod.GET)
    public List<StatisticHero> fill() {
        for (String name : Arrays.asList("Soft skills", "Technical skills")) {
            StatisticHero generalType = new StatisticHero(name);
            this.manager.create(generalType);
        }

        return this.getAll();
    }
}
