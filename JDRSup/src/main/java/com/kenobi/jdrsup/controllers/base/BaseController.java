package com.kenobi.jdrsup.controllers.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.kenobi.jdrsup.managers.interfaces.base.IBaseManager;
import com.kenobi.jdrsup.models.base.BaseEntity;



public class BaseController<T extends BaseEntity> {
	@Autowired
	private IBaseManager<T> baseCrud;

	private Class<T> clazz;

	/**
	 * @return the clazz
	 */
	protected Class<T> getClazz() {
		return clazz;
	}

	protected BaseController(Class<T> clazz){
		this.clazz = clazz;
	}

	public T insertItem(@ModelAttribute T item){
		baseCrud.create(item);
		return item;
	}

	public String updateItem(@ModelAttribute T item){
		try {
			baseCrud.update(item);
		} catch (Exception e) {
			return "Update failed";
		}
		return "Update OK";
	}


	public String deleteItem(@ModelAttribute T item){
		try {
			baseCrud.delete(item);
		} catch (Exception e) {
			return "Delete failed";
		}
		return "Delete OK";
	}

	public T getItem(Integer id){
		T item = null;
		item = baseCrud.getById(id);
		return item;
	}

	public List<T> getItems(){
		List<T> items = null;
		items = (List<T>) baseCrud.getAll();
		return items;
	}
}
