package com.kenobi.jdrsup.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.kenobi.jdrsup.models.base.BaseEntity;

@Entity
@Table(name = "profession")
public class Profession extends BaseEntity {

	@Column
	private String name;
	
	@OneToMany
	List<Skill> skills;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Skill> getSkills() {
		return skills;
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	
	
}
