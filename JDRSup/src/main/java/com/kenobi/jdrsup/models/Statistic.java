package com.kenobi.jdrsup.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.kenobi.jdrsup.models.base.BaseEntity;

@Entity
@Table(name = "statistic")
public class Statistic extends BaseEntity {

	@Column
	private String name;
	
	@OneToMany
	private List<StatisticHero> heroes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<StatisticHero> getHeroes() {
		return heroes;
	}

	public void setHeroes(List<StatisticHero> heroes) {
		this.heroes = heroes;
	}
	
	
	
}
