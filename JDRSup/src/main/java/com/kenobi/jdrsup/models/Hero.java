package com.kenobi.jdrsup.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.kenobi.jdrsup.models.base.BaseEntity;

@Entity
@Table(name = "hero")
public class Hero extends BaseEntity{

	@Column
	private String firstname;
	@Column
	private String lastname;
	@Column
	private String description;
	@Column
	private String background;
	@Column
	private int age;
	@Column
	private int exp;
	
	@OneToMany
	private List<SkillHero> skills;
	
	@OneToMany
	private List<StatisticHero> statistics;
	
	@ManyToOne
	private Race race;
	
	@ManyToOne
	private Profession profession;
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	public List<SkillHero> getSkills() {
		return skills;
	}
	public void setSkills(List<SkillHero> skills) {
		this.skills = skills;
	}
	public List<StatisticHero> getStatistics() {
		return statistics;
	}
	public void setStatistics(List<StatisticHero> statistics) {
		this.statistics = statistics;
	}
	public Race getRace() {
		return race;
	}
	public void setRace(Race race) {
		this.race = race;
	}
	public Profession getProfession() {
		return profession;
	}
	public void setProfession(Profession profession) {
		this.profession = profession;
	}
	
	
}
