package com.kenobi.jdrsup.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.kenobi.jdrsup.models.base.BaseEntity;

@Entity
@Table(name = "skill")
public class Skill extends BaseEntity {

	@Column
	private String name;
	
	@OneToMany
	private List<SkillHero> heroes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SkillHero> getHeroes() {
		return heroes;
	}

	public void setHeroes(List<SkillHero> heroes) {
		this.heroes = heroes;
	}

}
