package com.kenobi.jdrsup.models;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.kenobi.jdrsup.models.base.BaseEntity;

@Entity
@Table(name = "jdr")
public class JDR extends BaseEntity {
	@Column
	private String name;
	@Column
	private LocalDate dateBegin;
	
	@OneToMany
	private List<Hero> heroes;
	@OneToMany
	private List<Statistic> statistics;
	@OneToMany
	private List<Profession> professions;
	@OneToMany
	private List<Skill> skills;
	@OneToMany
	private List<Race> races;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDateBegin() {
		return dateBegin;
	}
	public void setDateBegin(LocalDate dateBegin) {
		this.dateBegin = dateBegin;
	}
	public List<Hero> getHeroes() {
		return heroes;
	}
	public void setHeroes(List<Hero> heroes) {
		this.heroes = heroes;
	}
	public List<Statistic> getStatistics() {
		return statistics;
	}
	public void setStatistics(List<Statistic> statistics) {
		this.statistics = statistics;
	}
	public List<Profession> getProfessions() {
		return professions;
	}
	public void setProfessions(List<Profession> professions) {
		this.professions = professions;
	}
	public List<Skill> getSkills() {
		return skills;
	}
	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	public List<Race> getRaces() {
		return races;
	}
	public void setRaces(List<Race> races) {
		this.races = races;
	}
	
}
