package com.kenobi.jdrsup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdrSupApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdrSupApplication.class, args);
	}
}
