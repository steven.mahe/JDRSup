package com.kenobi.jdrsup.dao.interfaces;

import org.springframework.stereotype.Repository;

import com.kenobi.jdrsup.dao.interfaces.base.IBaseDAO;
import com.kenobi.jdrsup.models.Skill;
@Repository
public interface ISkillDAO extends IBaseDAO<Skill>{

}
