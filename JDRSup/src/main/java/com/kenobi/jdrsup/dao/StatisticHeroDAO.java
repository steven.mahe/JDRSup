package com.kenobi.jdrsup.dao;

import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.base.BaseDAO;
import com.kenobi.jdrsup.dao.interfaces.IStatisticCharacterDAO;
import com.kenobi.jdrsup.models.StatisticHero;
@Transactional
public class StatisticHeroDAO extends BaseDAO<StatisticHero> implements IStatisticCharacterDAO{

	public StatisticHeroDAO(Class<StatisticHero> klazz) {
		super(klazz);
		// TODO Auto-generated constructor stub
	}

}
