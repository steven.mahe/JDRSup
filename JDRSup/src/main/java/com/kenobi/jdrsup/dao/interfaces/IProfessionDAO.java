package com.kenobi.jdrsup.dao.interfaces;

import org.springframework.stereotype.Repository;

import com.kenobi.jdrsup.dao.interfaces.base.IBaseDAO;
import com.kenobi.jdrsup.models.Profession;
@Repository
public interface IProfessionDAO extends IBaseDAO<Profession>{

}
