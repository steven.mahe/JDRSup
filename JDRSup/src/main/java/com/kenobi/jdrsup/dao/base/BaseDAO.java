package com.kenobi.jdrsup.dao.base;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.interfaces.base.IBaseDAO;
import com.kenobi.jdrsup.models.base.BaseEntity;

@Service
@Transactional
public class BaseDAO<T extends BaseEntity> implements IBaseDAO<T>  {

	private Class<T> klazz;

	@PersistenceContext
	protected EntityManager entityManager;

	public BaseDAO(Class<T> klazz) {
		super();
		this.klazz = klazz;
	}

	@Override
	public void create(T item) {
		entityManager.persist(item);
	}

	@Override
	public void delete(T item) {
		entityManager.detach(item);
	}

	@Override
	public List getAll() {
		return entityManager.createQuery("SELECT st FROM " + this.klazz.getSimpleName() + " st").getResultList();
	}

	@Override
	public T getById(Integer id) {
		return entityManager.find(this.klazz, id);
	}

	@Override
	public void update(T item) {
		entityManager.merge(item);
	}

	@Override
	public void delete(Integer id) {
		//TODO retrieve primary key name
		entityManager.createQuery("delete from " + this.klazz.getSimpleName() + " st where st." + "id" + " = :id")
			.setParameter("id", id)
			.executeUpdate();
	}

}
