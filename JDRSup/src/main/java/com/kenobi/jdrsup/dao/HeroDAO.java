package com.kenobi.jdrsup.dao;

import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.base.BaseDAO;
import com.kenobi.jdrsup.dao.interfaces.IHeroDAO;
import com.kenobi.jdrsup.models.Hero;
@Transactional
public class HeroDAO extends BaseDAO<Hero> implements IHeroDAO{

	public HeroDAO() {
		super(Hero.class);
	}

}
