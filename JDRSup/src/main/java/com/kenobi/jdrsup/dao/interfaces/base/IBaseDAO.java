package com.kenobi.jdrsup.dao.interfaces.base;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kenobi.jdrsup.models.base.BaseEntity;

@Service
public interface IBaseDAO<T extends BaseEntity> {
	public void create(T item);

	public void delete(T item);

	public void delete(Integer id);

	public List<T> getAll();

	public T getById(Integer id);

	public void update(T item);
}
