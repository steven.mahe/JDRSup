package com.kenobi.jdrsup.dao.interfaces;

import org.springframework.stereotype.Repository;

import com.kenobi.jdrsup.dao.interfaces.base.IBaseDAO;
import com.kenobi.jdrsup.models.Race;
@Repository
public interface IRaceDAO extends IBaseDAO<Race>{

}
