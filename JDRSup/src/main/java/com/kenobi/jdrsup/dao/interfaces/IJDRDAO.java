package com.kenobi.jdrsup.dao.interfaces;

import org.springframework.stereotype.Repository;

import com.kenobi.jdrsup.dao.interfaces.base.IBaseDAO;
import com.kenobi.jdrsup.models.JDR;

@Repository
public interface IJDRDAO extends IBaseDAO<JDR>{

}
