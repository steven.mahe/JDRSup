package com.kenobi.jdrsup.dao;

import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.base.BaseDAO;
import com.kenobi.jdrsup.dao.interfaces.IRaceDAO;
import com.kenobi.jdrsup.models.Race;
@Transactional
public class RaceDAO extends BaseDAO<Race> implements IRaceDAO{

	public RaceDAO(Class<Race> klazz) {
		super(klazz);
		// TODO Auto-generated constructor stub
	}

}
