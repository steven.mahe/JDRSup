package com.kenobi.jdrsup.dao;

import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.base.BaseDAO;
import com.kenobi.jdrsup.dao.interfaces.ISkillCHaracterDAO;
import com.kenobi.jdrsup.models.SkillHero;
@Transactional
public class SkillHeroDAO extends BaseDAO<SkillHero> implements ISkillCHaracterDAO{

	public SkillHeroDAO(Class<SkillHero> klazz) {
		super(klazz);
		// TODO Auto-generated constructor stub
	}

}
