package com.kenobi.jdrsup.dao;

import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.base.BaseDAO;
import com.kenobi.jdrsup.dao.interfaces.IProfessionDAO;
import com.kenobi.jdrsup.models.Profession;
@Transactional
public class ProfessionDAO extends BaseDAO<Profession> implements IProfessionDAO{

	public ProfessionDAO(Class<Profession> klazz) {
		super(klazz);
		// TODO Auto-generated constructor stub
	}

}
